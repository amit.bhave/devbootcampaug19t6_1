package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Observable;

public class GrudgerBehaviourTest {
    GrudgerBehaviour grudgerBehaviour;

    @Before
    public void shouldCreateInstance(){
        grudgerBehaviour = new GrudgerBehaviour();
    }

    @Test
    public void shouldGrudgerCoopOnFirstMove(){
        Assert.assertEquals(Move.COOPERATE, grudgerBehaviour.move());
    }

    @Test
    public void shouldGrudgerCheatAfterOpponentCheats(){
        grudgerBehaviour.move();
        grudgerBehaviour.update(new Observable(), Move.CHEAT);

        Move move = grudgerBehaviour.move();
        Assert.assertEquals(Move.CHEAT, move);
    }
}
