package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class PlayerTest {


    @Test
    public void ShouldPlayerBeWithCheaterBehaviour() {
        Behaviour cheaterBehaviour = () -> {
            return Move.CHEAT;
        };
        Player player = new Player(cheaterBehaviour);
        Assert.assertNotNull(player);
        Assert.assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void ShouldPlayerBeWithCooperateBehaviour() {
        Behaviour cooperativeBehaviour = () -> {
            return Move.COOPERATE;
        };
        Player player = new Player(cooperativeBehaviour);
        Assert.assertNotNull(player);
        Assert.assertEquals(Move.COOPERATE, player.move());
    }

    @Test
    public void getInstanceOfPlayerWithConsoleBehaviour() {
        Behaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("1"));
        Player player = new Player(consoleBehaviour);
        Assert.assertNotNull(player);
    }

    @Test
    public void getCurrentScore() {
        Behaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("1"));
        Player player = new Player(consoleBehaviour);
        Assert.assertEquals(0, player.score());
    }

    @Test
    public void shouldBeAbleToUpdateScore() {
        Behaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("1"));
        Player player = new Player(consoleBehaviour);
        player.addScore(2);
        Assert.assertEquals(2, player.score());
        player.addScore(-1);
        Assert.assertEquals(1, player.score());
    }

}
