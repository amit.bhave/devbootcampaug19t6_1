package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class ConsoleBehaviourTest {

    @Test
    public void ableToReadInput() {
        Behaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("1"));
        Player player = new Player(consoleBehaviour);
        Assert.assertEquals(Move.COOPERATE, player.move());

        Behaviour consoleBehaviour2 = new ConsoleBehaviour(new Scanner("0"));
        Player player1 = new Player(consoleBehaviour2);
        Assert.assertEquals(Move.CHEAT, player1.move());
    }
}
