package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;

import static com.dev.bootcamp.Move.CHEAT;
import static com.dev.bootcamp.Move.COOPERATE;
import static org.mockito.Mockito.*;

public class GameTest {

    private Player player1 = mock(Player.class);
    private Player player2 = mock(Player.class);
    private Machine machine = mock(Machine.class);
    private PrintStream  console = mock(PrintStream.class);

    @Before
    public void setup() {
        when(player1.move()).thenReturn(COOPERATE);
        when(player2.move()).thenReturn(COOPERATE);
        when(machine.scoreFor(new Move[]{COOPERATE,COOPERATE})).thenReturn(new int[]{2, 2});
    }

    @Test
    public void shouldReturnInstance() {
        Game game = new Game(player1, player2, machine, 1, System.out);
        Assert.assertNotNull(game);
    }

    @Test
    public void shouldGameHavePlay() {
        Game game = new Game(player1, player2, machine, 1, System.out);
        game.play();
    }

    @Test
    public void playersShouldAbleToMove() {
        Game game = new Game(player1, player2, machine, 1, System.out);
        game.play();
        verify(player1).move();
        verify(player2).move();
    }

    @Test
    public void machineShouldBeAbleToCalculateTheScore() {
        Game game = new Game(player1, player2, machine, 1, System.out);
        game.play();
        verify(machine).scoreFor(any());
    }

    @Test
    public void playersScoreShouldBeUpdated() {
        Game game = new Game(player1, player2, machine, 1, System.out);
        when(player1.move()).thenReturn(COOPERATE);
        when(player2.move()).thenReturn(COOPERATE);
        when(machine.scoreFor(new Move[]{COOPERATE,COOPERATE})).thenReturn(new int[]{2, 2});
        game.play();
        verify(player1).addScore(2);
        verify(player2).addScore(2);
    }

    @Test
    public void playersScoreShouldBeUpdatedForCooperateCheat() {
        Game game = new Game(player1, player2, machine, 1, System.out);
        when(player1.move()).thenReturn(COOPERATE);
        when(player2.move()).thenReturn(CHEAT);
        when(machine.scoreFor(new Move[]{COOPERATE,CHEAT})).thenReturn(new int[]{-1, 3});
        game.play();
        verify(player1).addScore(-1);
        verify(player2).addScore(3);
    }

    @Test
    public void playerScoreShouldBeUpdatedForMultipleRounds() {
        Game game = new Game(player1, player2, machine, 2, System.out);
        when(player1.move()).thenReturn(COOPERATE).thenReturn(CHEAT);
        when(player2.move()).thenReturn(CHEAT).thenReturn(COOPERATE);
        when(machine.scoreFor(new Move[]{COOPERATE, CHEAT})).thenReturn(new int[]{-1, 3});
        when(machine.scoreFor(new Move[]{CHEAT, COOPERATE})).thenReturn(new int[]{3, -1});

        game.play();
        verify(player1).addScore(-1);
        verify(player2).addScore(3);

        verify(player1).addScore(3);
        verify(player2).addScore(-1);

    }

    // for 5 rounds run for cheater and coop and validate teh results
    @Test
    public void validatePlayersScoreForCheaterAndCooperatorForFiveRounds() {
        Behaviour cooperativeBehaviour = () -> {
            return Move.COOPERATE;
        };
        Player cooperative = new Player(cooperativeBehaviour);

        Behaviour cheaterBehaviour = () -> {
            return Move.CHEAT;
        };
        Player cheater = new Player(cheaterBehaviour);

        Machine machine = new Machine();

        Game game = new Game(cooperative, cheater, machine, 5, System.out);

        game.play();

        Assert.assertEquals(-5, cooperative.score());
        Assert.assertEquals(15, cheater.score());
    }

    @Test
    public void shouldValidateScoreForCopycatAndCheatPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cheater = new Player(() -> Move.CHEAT);
        Machine machine = new Machine();
        Game game = new Game(copycat, cheater, machine, 2, System.out);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(-1, copycat.score());
        Assert.assertEquals(3, cheater.score());

    }

    @Test
    public void shouldValidateScoreForCheatAndCopycatPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cheater = new Player(() -> Move.CHEAT);
        Machine machine = new Machine();
        Game game = new Game(cheater, copycat, machine, 2, System.out);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(-1, copycat.score());
        Assert.assertEquals(3, cheater.score());

    }

    @Test
    public void shouldValidateScoreForCooperateAndCopycatPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cooperate = new Player(() -> COOPERATE);
        Machine machine = new Machine();
        Game game = new Game(cooperate, copycat, machine, 2, System.out);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(4, copycat.score());
        Assert.assertEquals(4, cooperate.score());

    }

    @Test
    public void shouldValidateScoreForCopycatAndCooperatePlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cooperate = new Player(() -> COOPERATE);
        Machine machine = new Machine();
        Game game = new Game(cooperate, copycat, machine, 2, System.out);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(4, copycat.score());
        Assert.assertEquals(4, cooperate.score());

    }

    @Test
    public void shouldValidateScoreForGrudgerAndCopyCatPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        GrudgerBehaviour grudgerBehaviour = new GrudgerBehaviour();
        Player grudger = new Player(grudgerBehaviour);
        Game game = new Game(grudger, copycat, new Machine(), 4, console);
        game.addObserver(copycatBehaviour);
        game.addObserver(grudgerBehaviour);

        game.play();

        Mockito.verify(console).println("Player 1 : 2 Player 2 : 2");
        Mockito.verify(console).println("Player 1 : 4 Player 2 : 4");
        Mockito.verify(console).println("Player 1 : 6 Player 2 : 6");
        Mockito.verify(console).println("Player 1 : 8 Player 2 : 8");
        Assert.assertEquals(8, copycat.score());
        Assert.assertEquals(8, grudger.score());

    }

    @Test
    public void shouldValidateScoreForGrudgerAndCheatPlayers() {
        Player cheater = new Player(()-> CHEAT);
        GrudgerBehaviour grudgerBehaviour = new GrudgerBehaviour();
        Player grudger = new Player(grudgerBehaviour);
        Game game = new Game(grudger, cheater, new Machine(), 4, console);
        game.addObserver(grudgerBehaviour);

        game.play();

        Mockito.verify(console, times(4)).println("Player 1 : -1 Player 2 : 3");
        Mockito.verify(console, times(4)).println("Player 1 : -1 Player 2 : 3");
        Mockito.verify(console, times(4)).println("Player 1 : -1 Player 2 : 3");
        Mockito.verify(console, times(4)).println("Player 1 : -1 Player 2 : 3");
        Assert.assertEquals(3, cheater.score());
        Assert.assertEquals(-1, grudger.score());
    }

    @Test
    public void shouldValidateScoreForDetectiveAndCooperatePlayers() {
        Player cooperatePlayer = new Player(()-> COOPERATE);
        DetectiveBehavior detectiveBehaviour = new DetectiveBehavior();
        Player detective = new Player(detectiveBehaviour);
        Game game = new Game(detective, cooperatePlayer, new Machine(), 6, console);
        game.addObserver(detectiveBehaviour);

        game.play();

        Mockito.verify(console).println("Player 1 : 2 Player 2 : 2");
        Mockito.verify(console).println("Player 1 : 5 Player 2 : 1");
        Mockito.verify(console).println("Player 1 : 7 Player 2 : 3");
        Mockito.verify(console).println("Player 1 : 9 Player 2 : 5");
        Mockito.verify(console).println("Player 1 : 12 Player 2 : 4");
        Mockito.verify(console).println("Player 1 : 15 Player 2 : 3");
        Assert.assertEquals(3, cooperatePlayer.score());
        Assert.assertEquals(15, detective.score());
    }

    @Test
    public void shouldValidateScoreForDetectiveAndCheatPlayers() {
        Player cheatPlayer = new Player(()-> CHEAT);
        DetectiveBehavior detectiveBehaviour = new DetectiveBehavior();
        Player detective = new Player(detectiveBehaviour);
        Game game = new Game(detective, cheatPlayer, new Machine(), 6, console);
        game.addObserver(detectiveBehaviour);

        game.play();

        Mockito.verify(console, times(2)).println("Player 1 : -1 Player 2 : 3");
        Mockito.verify(console, times(2)).println("Player 1 : -1 Player 2 : 3");
        Mockito.verify(console).println("Player 1 : -2 Player 2 : 6");
        Mockito.verify(console, times(3)).println("Player 1 : -3 Player 2 : 9");
        Mockito.verify(console, times(3)).println("Player 1 : -3 Player 2 : 9");
        Mockito.verify(console, times(3)).println("Player 1 : -3 Player 2 : 9");
        Assert.assertEquals(9, cheatPlayer.score());
        Assert.assertEquals(-3, detective.score());
    }



}
