package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Observable;

public class DetectiveBehaviorTest {

    DetectiveBehavior detectiveBehavior;

    @Before
    public void setUp(){
        detectiveBehavior =  new DetectiveBehavior();
    }

    @Test
    public void shouldValidateFirstFourMove(){
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
        Assert.assertEquals(Move.CHEAT,detectiveBehavior.move());
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
    }

    @Test
    public void shouldValidateFifthMoveWhenOpponentNeverCheated(){
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
        Assert.assertEquals(Move.CHEAT,detectiveBehavior.move());
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
        detectiveBehavior.update(new Observable(), Move.COOPERATE);
        Assert.assertEquals(Move.CHEAT,detectiveBehavior.move());
    }

    @Test
    public void shouldValidateFifthMoveWhenOpponentAlwaysCheated(){
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
        Assert.assertEquals(Move.CHEAT,detectiveBehavior.move());
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
        Assert.assertEquals(Move.COOPERATE,detectiveBehavior.move());
        detectiveBehavior.update(new Observable(), Move.CHEAT);
        Assert.assertEquals(Move.CHEAT,detectiveBehavior.move());
    }


}
