package com.dev.bootcamp;

import java.util.Observable;
import java.util.Observer;

public class CopycatBehaviour implements Behaviour, Observer {
    private Move opponentPreviousMove = Move.COOPERATE;
    @Override
    public Move move() {
        return opponentPreviousMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        opponentPreviousMove = (Move) arg;
    }
}
