package com.dev.bootcamp;

import java.util.Observable;
import java.util.Observer;

public class GrudgerBehaviour implements Behaviour, Observer {

    private Move opponentPreviousMove = Move.COOPERATE;

    public Move move() {
        return opponentPreviousMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        if((Move) arg == Move.CHEAT) {
            opponentPreviousMove = (Move) arg;
            o.deleteObserver(this);
        }
    }
}
