package com.dev.bootcamp;

import java.io.PrintStream;
import java.util.Observable;

public class Game extends Observable {

    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final int noOfRounds;
    private PrintStream printStream;

    public Game(Player player1, Player player2, Machine machine, int noOfRounds, PrintStream printStream) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
        this.printStream = printStream;
    }

    public void play() {
        Behaviour player1Behaviour = this.player1.getBehaviour();
        Behaviour player2Behaviour = this.player2.getBehaviour();
        for (int i = 0; i < noOfRounds; i++) {
            Move player1Move = this.player1.move();
            Move player2Move = this.player2.move();
            notifyOpponentMove(player1Behaviour, player2Move);
            notifyOpponentMove(player2Behaviour, player1Move);
            int[] scores = this.machine.scoreFor(new Move[]{player1Move, player2Move});
            player1.addScore(scores[0]);
            player2.addScore(scores[1]);

            printStream.println("Player 1 : " + player1.score() + " Player 2 : " + player2.score());
        }
    }

    private void notifyOpponentMove(Behaviour playerBehaviour, Move playerMove) {
        if (playerBehaviour instanceof CopycatBehaviour
                || playerBehaviour instanceof GrudgerBehaviour
                || playerBehaviour instanceof DetectiveBehavior) {
            notifyOpponentsPreviousMove(playerMove);
        }
    }

    private void notifyOpponentsPreviousMove(Move opponentsMove) {
        setChanged();
        notifyObservers(opponentsMove);
    }

}
