package com.dev.bootcamp;

import static com.dev.bootcamp.Move.CHEAT;

public class Application {

    public static void main(String[] args) {
        /*CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cheater = new Player(() -> Move.CHEAT);
        Machine machine = new Machine();
        Game game = new Game(copycat, cheater, machine, 2, System.out);
        game.addObserver(copycatBehaviour);
        game.play();*/

        Player cheater = new Player(()-> CHEAT);
        GrudgerBehaviour grudgerBehaviour = new GrudgerBehaviour();
        Player grudger = new Player(grudgerBehaviour);
        Game game = new Game(grudger, cheater, new Machine(), 4, System.out);
        game.addObserver(grudgerBehaviour);

        game.play();
    }

}
