package com.dev.bootcamp;

public class Player {

    private Behaviour playerBehaviour;
    private int score;

    public Player(Behaviour playerBehaviour) {
        this.playerBehaviour = playerBehaviour;
    }

    public Move move() {
        return playerBehaviour.move();
    }

    public int score() {
        return this.score;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public Behaviour getBehaviour() {
        return this.playerBehaviour;
    }

}
