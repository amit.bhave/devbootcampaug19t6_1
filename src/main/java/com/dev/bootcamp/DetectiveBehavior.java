package com.dev.bootcamp;

import java.util.Observable;
import java.util.Observer;

    public class DetectiveBehavior implements Behaviour, Observer {

    private static Move[] moves = new Move[] {Move.COOPERATE, Move.CHEAT, Move.COOPERATE, Move.COOPERATE};
    private static int moveIndex = 0;
    private Move opponentMove;
    int cheatCounter = 0;
    public Move move() {
        if(moveIndex<=3) {
            return moves[moveIndex++];
        }
        else if(cheatCounter == 0){
            return Move.CHEAT;
        }
        else{
            return opponentMove;
        }
    }

    @Override
    public void update(Observable o, Object oponentMove) {
        if(Move.CHEAT == (Move) opponentMove){
            cheatCounter++;
        }
        this.opponentMove = opponentMove;
    }
}
